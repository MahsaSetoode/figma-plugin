import React, { useState } from 'react';
import styles from './ui.module.scss';
import ApplyButton from './components/ApplyButton';
import RemoveButton from './components/RemoveButton';

const UI = ({}) => {
  const [svmState, setSvmState] = useState(false);
  const textbox = React.useRef(undefined);

  const onCreate = () => {
    console.log(svmState);
    if (svmState) {
      parent.postMessage({ pluginMessage: { type: 'not-create-svg' } }, '*');
      return;
    }
    setSvmState(true);
    parent.postMessage({ pluginMessage: { type: 'create-svg' } }, '*');
  };

  const onRemove = () => {
    if (svmState) {
      parent.postMessage({ pluginMessage: { type: 'remove-svg' } }, '*');
      return;
    }
    parent.postMessage({ pluginMessage: { type: 'not-remove-svg' } }, '*');
  };

  React.useEffect(() => {
    // This is how we read messages sent from the plugin controller
    window.onmessage = (event) => {
      const { type, message } = event.data.pluginMessage;
      if (type === 'create-svg') {
        console.log(`Figma Says: ${message}`);
      }
    };
  }, []);

  return (
    <div className={styles.buttonContainer}>
      <ApplyButton onClick={onCreate}>Apply SVG</ApplyButton>
      <RemoveButton onClick={onRemove}>Remove SVG</RemoveButton>
    </div>
  );
};

export default UI;
