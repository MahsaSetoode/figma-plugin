// import SVM from '../app/assets';
figma.showUI(__html__);

figma.ui.onmessage = (msg) => {
  if (msg.type === 'create-svg') {
    // var svg = document.createElementNS(
    //   'https://icon.fra1.cdn.digitaloceanspaces.com/Bootstrap%20Icons/icons/alarm-fill.svg',
    //   'svg'
    // );
    const rect = figma.createRectangle();
    // rect.id = 'alarm';
    rect.fillGeometry = '../assets/alarm-fill.svg'; //create block of img
    // rect.backGround
    figma.currentPage.appendChild(rect);

    //TODO
    // show svg on page
    // figma.currentPage = await self.fetch(`${svg}${width}x${height}`)

    // This is how figma responds back to the ui
    // figma.ui.postMessage({
    //   type: 'create-svg',
    //   message: `SVG Added`,
    // });
    figma.closePlugin('SVG added!');
  } else if (msg.type === 'remove-svg') {
    //TODO
    // delete svg
    // const rem = document.querySelector('#alarm');
    // rem.removeChild;
    const find = figma.root.findOne((el) => el.id === 'alarm');
    find.remove();
    figma.closePlugin('SVG removed!');
  } else {
    figma.notify("Action doesn't valid!");
  }
};
